FoorumApp.controller('ShowTopicController', function($scope, $routeParams, $location, Api){
  // Toteuta kontrolleri tähän
  Api.getTopic($routeParams.id).success(function(response){
      if (response == null)
        response = [];
      $scope.topic = response;
      Api.getMessages($routeParams.id).success(function(response){
        $scope.messages = response;
      }).error(function(data,status,headers,config){});
  }).error(function(data,status,headers,config){
    console.log(data);
  });
    $scope.addMessage = function() {
    Api.addMessage($scope.newMessage,$routeParams.id).success(function(response){
        $scope.newMessage.header = "";
        $scope.newMessage.message = "";
        location.reload();
        console.log(response);
        console.log(response.data);
    }).error(function(data,status,headers,config){
      if (status == 403) {
        console.log("Käyttäjä ei ole kirjautunut sisään.");
      }
      console.log(data);
      console.log(status);
      console.log(headers);
      console.log(config);}
      );
    };
});
