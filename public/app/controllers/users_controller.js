FoorumApp.controller('UsersController', function($scope, $location, Api){
  // Toteuta kontrolleri tähän
  $scope.login = function() {
    Api.login($scope.user).success(function(response){
      console.log(response);
      $scope.errorMessage = '';
      $location.path('/');
    }).error(function(data,status,headers,config){
      console.log(data);
      $scope.errorMessageLogin = data + ' ' + status;
      $scope.user.password = '';
      $scope.user.passwordCheck = '';
    });
  };
  $scope.register = function() {
    Api.register($scope.user).success(function(response){
      console.log(response);
      $location.path('/');
    }).error(function(data,status,headers,config){
      console.log(data);
      $scope.errorMessageRegister = data.error;
      $scope.user.password = '';
      $scope.user.passwordCheck = '';
    });
  };
  Api.getUserLoggedIn().success(function(response){
    console.log(response);
  }).error(function(data,status,headers,config){
    console.log(data);
  });
  Api.logout().success(function(response){
    console.log(response);
  }).error(function(data,status,headers,config){
    console.log(data);
  });
});
