FoorumApp.controller('ShowMessageController', function($scope, $routeParams, Api){
  // Toteuta kontrolleri tähän
  Api.getMessage($routeParams.id).success(function(response){
    console.log("Api.getMessages(id)");
      if (response == null)
        response = [];
      $scope.message = response;
      Api.getReplies($routeParams.id).success(function(response){
        console.log("Api.getReplies(id)");
      if (response == null)
        response = [];
        $scope.replies = response;
      }).error(function(data,status,headers,config){});
  }).error(function(data,status,headers,config){
    console.log(data);
  });
    $scope.addReply = function() {
    Api.addReply($scope.newReply,$routeParams.id).success(function(response){
        $scope.newReply.message = "";
        location.reload();
    }).error(function(data,status,headers,config){
      console.log(data);
    });
    }
});
