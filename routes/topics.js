var express = require('express');
var router = express.Router();

var authentication = require('../utils/authentication');
var Models = require('../models');

var date = new Date();

var mongodb = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectId;
var uri = "mongodb://root:M2NkkanB@ds233763.mlab.com:33763/cloud";

// GET /topics
router.get('/', function(req, res, next) {
  mongodb.connect(uri, function(err, client) {
    if(err) throw err;
    let db = client.db('cloud');
    db.collection('topics').find({}).toArray(function(err, result) {
      if(err) throw err;
      res.json(result);
    });
  });
});

// GET /topics/:id
router.get('/:id', function(req, res, next) {
  mongodb.connect(uri, function(err, client) {
    if(err) throw err;
    let db = client.db('cloud');
    db.collection('topics').findOne({ '_id' : ObjectId(req.params.id) },function(err, result) {
      if(err) throw err;
      console.log(result);
      res.json(result);
    });
  });
});

// POST /topics
router.post('/', authentication, function(req, res, next) {
  var topicToAdd = req.body;
  mongodb.connect(uri, function(err, client) {
    if(err) throw err;
    let db = client.db('cloud');
    db.collection('topics').insert(topicToAdd, function(err, result) {
      if(err) throw err;
      res.send(201);
    });
  });
});

// POST /topics/:id/messages
router.post('/:id/messages', authentication, function(req, res, next) {
  var messageToAdd = req.body;
  mongodb.connect(uri, function(err, client) {
    let db = client.db('cloud');
    if(err) throw err;
    messageToAdd.id = req.params.id;
    messageToAdd.date = (new Date()).toString();
    messageToAdd.TopicId = req.params.id;
    db.collection('users').findOne({ '_id' : ObjectId(req.session.userId) },function(err, result) {
      if(err) throw err;
      messageToAdd.user = result.username;
      db.collection('messages').insertOne(messageToAdd, function(err, result) {
        if(err) throw err;
        res.send(201);
      });
    });
  });});

// GET /topics/:id/messages
router.get('/:id/messages', function(req, res, next) {
  mongodb.connect(uri, function(err, client) {
    if(err) throw err;
    let db = client.db('cloud');
    db.collection('messages').find({ 'id' : req.params.id }).toArray(function(err, result) {
      if(err) throw err;
      res.json(result);
    });
  });
});

module.exports = router;
