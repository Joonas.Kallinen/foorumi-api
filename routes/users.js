var express = require('express');
var router = express.Router();

var Models = require('../models');

var mongodb = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectId;
var uri = "mongodb://root:M2NkkanB@ds233763.mlab.com:33763/cloud";

// Huom! Kaikki polut alkavat polulla /users

// POST /users
router.post('/', function(req, res, next){
  // Lisää tämä käyttäjä (Vinkki: create), muista kuitenkin sitä ennen varmistaa, että käyttäjänimi ei ole jo käytössä! (Vinkki: findOne)
  var userToAdd = req.body;
  if (userToAdd.password != userToAdd.passwordCheck) {
    res.status(400).json({ error: 'Salasanat eivät täsmää!' });
    return;
  }
  // Palauta vastauksena lisätty käyttäjä
  console.log(req.body);
  mongodb.connect(uri, function(err, client) {
    if(err) throw err;
    let db = client.db('cloud');
    db.collection('users').find({ 'username' : userToAdd.username }).toArray(function(err, result) {
      console.log(err);
      if(result.length == 0) {
        db.collection('users').insertOne(userToAdd, function(err, result) {
          if(err) throw err;
          req.session.userId = userToAdd._id;
          res.json(userToAdd);
        });
      } else {
        res.status(400).json({ error: 'Käyttäjätunnus on jo käytössä!' });
      }
    });
  });
});

// POST /users/authenticate
router.post('/authenticate', function(req, res, next){
  // Tarkista käyttäjän kirjautuminen tässä. Tee se katsomalla, löytyykö käyttäjää annetulla käyttäjätunnuksella ja salasanalla (Vinkki: findOne ja sopiva where)
  var userToCheck = req.body;
  if(userToCheck == null || userToCheck.username == null || userToCheck.password == null){
    res.send(403);
    return;
  }
  mongodb.connect(uri, function(err, client) {
    if(err) throw err;
    let db = client.db('cloud');
    db.collection('users').find({ 'username' : userToCheck.username, 'password' : userToCheck.password }).toArray(function(err, result) {
      if(err) throw err;
      if(result.length == 1) {
        req.session.userId = result[0]._id;
        res.json(result);
      } else {
        res.send(403);
      }
    });
  });
});

// GET /users/logged-in
router.get('/logged-in', function(req, res, next){
  var loggedInId = req.session.userId ? req.session.userId : null;
  console.log('loggedInId:');
  console.log(loggedInId);
  if(loggedInId == null){
    res.json({});
  } else {
    // Hae käyttäjä loggedInId-muuttujan arvon perusteella (Vinkki: findOne)
    mongodb.connect(uri, function(err, client) {
      if(err) throw err;
      let db = client.db('cloud');
      db.collection('users').findOne({ '_id' : ObjectId(loggedInId) },function(err, result) {
        if(err) throw err;
      console.log('result:');
      console.log(result);
      console.log(result.length);
      if(result) {
        console.log('result:');
        console.log(result);
        res.json(result);
      } else {
        res.send(403);
      }
      });
    });
  }
});

// GET /users/logout
router.get('/logout', function(req, res, next){
  req.session.userId = null;

  res.send(200);
});

module.exports = router;
