var express = require('express');
var router = express.Router();

var authentication = require('../utils/authentication');
var Models = require('../models');

var date = new Date();

var mongodb = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectId;
var uri = "mongodb://root:M2NkkanB@ds233763.mlab.com:33763/cloud";

// Huom! Kaikki polut alkavat polulla /messages

// GET /messages/:id
router.get('/:id', function(req, res, next) {
    console.log('!! ' + req.params.id);
  mongodb.connect(uri, function(err, client) {
    if(err) throw err;
    let db = client.db('cloud');
    db.collection('messages').findOne({ '_id' : ObjectId(req.params.id) },function(err, result) {
      if(err) throw err;
      res.json(result);
    });
  });
});

router.get('/:id/replies', function(req, res, next) {
  mongodb.connect(uri, function(err, client) {
    if(err) throw err;
    let db = client.db('cloud');
    db.collection('replies').find({ 'id' : req.params.id }).toArray(function(err, result) {
      if(err) throw err;
      res.json(result);
    });
  });
});

// POST /messages/:id/reply
router.post('/:id/replies', authentication, function(req, res, next) {
    console.log("# replies toimii #");
  var replyToAdd = req.body;
  replyToAdd.id = req.params.id;
  replyToAdd.date = (new Date()).toString();
  replyToAdd.MessageId = req.params.id;
  mongodb.connect(uri, function(err, client) {
    if(err) throw err;
    let db = client.db('cloud');
    db.collection('users').findOne({ '_id' : ObjectId(req.session.userId) },function(err, result) {
      if(err) throw err;
      replyToAdd.user = result.username;
      db.collection('replies').insertOne(replyToAdd, function(err, result) {
        if(err) throw err;
        res.send(201);
      });
    });
  });});


module.exports = router;
